<?php
use yii\helpers\Html;
?>
<p>You have entered the following information:</p>

<ul class="list-group">
    <li class="list-group-item"><label for="name">Name</label>: <?= Html::encode($model->name)?></li>
    <li class="list-group-item"><label for="email">Email</label>: <?= Html::encode($model->email)?></li>
</ul>
