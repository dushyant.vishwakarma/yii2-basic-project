<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Countries</h1>

<ul class="list-group">
    <?php foreach ($countries as $country): ?>
        <li class="list-group-item">
            <?= Html::encode("{$country->code} ({$country->name})") ?>:
            <?= $country->population ?>
        </li>
    <?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
